const faker = require("faker");
const { UserTC, UserSchema } = require("../model/user");
const resolver = function () {};
resolver.fakeData = UserTC.addResolver({
  name: "user",
  type: UserTC,
  args: { record: UserTC.getInputType() },
  resolve: async ({ source, args }) => {
    let user = new UserSchema({
      name: faker.name.firstName(),
      lastname: faker.name.lastName(),
      email: faker.internet.email(),
      phone: faker.phone.phoneNumber(),
      profileImage: faker.random.image(),
      age: Math.floor(
        Math.random() * (100 - 1) + 1
      )
    });
    return await user.save();
  },
});

module.exports = resolver;
