const request = require('supertest');
const app = require('./server');
// const mongoose = require('mongoose')
// afterAll(async () => {
//   await mongoose.connection.close()
// })
describe('App Request', () => {
  test('should responds with 404', async (done) => {
    const result = await request(app).get('/');
    expect(result.status).toBe(200);
    done();
  });
});