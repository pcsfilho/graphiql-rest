const express = require("express");
const app = express();
const db = require("./database/config");
const mongoose = require("mongoose");
const { graphqlHTTP } = require("express-graphql");
const logger = require("./core/logger");
// use it before all route definitions
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  if (req.method === "OPTIONS") {
    return res.sendStatus(200);
  }
  // Pass to next layer of middleware
  next();
});
// const extensions = ({ context }) => {
//   return {
//     runTime: Date.now() - context.startTime,
//   };
// };

// app.use(logger);

app.listen(5000, async () => {
  // console.log("server is running ", 5000);
  await mongoose.connect(db.uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
});

mongoose.connection.on(
  "error",
  console.error.bind(console, "MongoDB connection error:")
);
const graphqlSchema = require("./schemas/index");
app.use(
  "/graphql",
  graphqlHTTP((request) => {
    return {
      context: { startTime: Date.now() },
      graphiql: true,
      schema: graphqlSchema
      // extensions,
    };
  })
);

app.use(require("./routes"));
module.exports = app
